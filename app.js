'use strict';

const path = require('path');
const types = require('./lib/types');
const {
  readDirectory
} = require('./lib/util');

module.exports = app => {
  app.coreLogger.info('[egg-knex-model] egg-knex-model begin start');

  const start = Date.now();

  const config = app.config.knexModel;

  app.model = {};

  function map(files, client) {
    if(!files || !files.length) {
      app.coreLogger.error('[egg-knex-model] not find "%s", egg-knex-model start fail', config.directory);

      return;
    }

    if(client) app.model[client] = {};

    const knex = client ? app.knex.get(client) : app.knex;

    const tables = [];

    files.forEach(({ filepath }, i) => {
      const name = path.basename(filepath);

      const ext = path.extname(name);

      const key = name.replace(ext, '');

      const Model = app.loader.loadFile(filepath);

      const model = new Model(knex, types);

      tables[(model.position || (i + 1)) - 1] = model.createTable();

      if(client)
        app.model[client][key] = model;
      else app[key] = model;
    });

    Promise.all(tables);
  }

  const directory = path.join(app.config.baseDir, config.directory);

  const directories = readDirectory(directory, [ 'directory' ]);

  if(!directories || !directories.length) {
    const files = readDirectory(directory, [ 'file' ]);

    map(files);
  }else
    directories.forEach(({ filepath }) => {
      const client = path.basename(filepath);

      const files = readDirectory(filepath, [ 'file' ]);

      map(files, client);
    });

  app.model.get = (name, client) => {
    return client
      ? app.model[client][name]
      : app.model[name];
  };

  app.coreLogger.info('[egg-knex-model] egg-knex-model started use %d ms', Date.now() - start);
};
