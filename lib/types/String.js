'use strict';

const Type = require('./Type');

class StringType extends Type {
  static type(table, name, maxLength = 255) {
    return table.string(name, maxLength);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const isString = typeof value === 'string' || typeof value === 'number';

    if(!value || !isString)
      return undefined;

    return String(value);
  }
}

module.exports = StringType;
