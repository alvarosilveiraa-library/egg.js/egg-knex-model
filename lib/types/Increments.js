'use strict';

const Type = require('./Type');

class IncrementsType extends Type {
  static type(table, name) {
    return table.increments(name).unsigned().primary();
  }

  constructor(options = {}) {
    super({
      ...options,
      selectable: true,
      insertable: false,
      updatable: false,
      deletable: true
    });
  }

  valueOf(value) {
    const isNumber = !isNaN(value);

    if(!isNumber || (!value && !isNumber))
      return undefined;

    return Number(value);
  }
}

module.exports = IncrementsType;
