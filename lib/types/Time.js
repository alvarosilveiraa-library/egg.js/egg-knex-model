'use strict';

const Type = require('./Type');

class TimeType extends Type {
  static type(table, name) {
    return table.time(name);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const regexp = /^([01][0-9]|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)$/;

    if(!value || !regexp.test(value))
      return undefined;

    return value;
  }
}

module.exports = TimeType;
