'use strict';

/* eslint no-bitwise: 0 */

const Type = require('./Type');

const dec2ArrayBits = dec => {
  const bites = [];

  for(let i = 0; i < 8; i++) {
    const actived = (dec & (1 << i)) !== 0;

    bites.push(actived ? 1 : 0);
  }

  return bites;
};

const dec2ObjectBits = (dec, keys = []) => {
  const bites = {};

  for(let i = 0; i < 8; i++) {
    const actived = (dec & (1 << i)) !== 0;

    if(keys[i]) bites[keys[i]] = actived ? 1 : 0;
  }

  return bites;
};

const dec2Bits = (dec, keys) => {
  if(keys && Array.isArray(keys))
    return dec2ObjectBits(dec, keys);

  return dec2ArrayBits(dec);
};

const arrayBits2Dec = bits => {
  let dec = 0;

  for(let i = 0; i < bits.length; i++)
    dec += Math.pow(2, i) * bits[i];

  return dec;
};

const objectBits2Dec = (bits, keys) => {
  let dec = 0;

  for(let i = 0; i < keys.length; i++)
    dec += Math.pow(2, i) * (bits[keys[i]] || 0);

  return dec;
};

const bits2Dec = (bits, keys) => {
  if(!Array.isArray(bits) && keys && Array.isArray(keys))
    return objectBits2Dec(bits, keys);

  return arrayBits2Dec(bits);
};

class BitterType extends Type {
  static type(table, name) {
    return table.integer(name);
  }

  constructor(options = {}) {
    super(options);

    this.keys = options.keys || [];
  }

  method(method) {
    return {
      select: value => this.decrypt(this.onValue(value)),
      insert: value => this.encrypt(this.onValue(value)),
      update: value => this.encrypt(this.onValue(value)),
      delete: value => this.valueOf(this.onValue(value))
    }[method];
  }

  encrypt(value) {
    const valueOf = this.valueOf(value);

    if(!valueOf || typeof valueOf !== 'object') return valueOf;

    return bits2Dec(valueOf, this.keys);
  }

  decrypt(value) {
    const valueOf = this.valueOf(value);

    if(typeof valueOf !== 'number') return valueOf;

    return dec2Bits(valueOf, this.keys);
  }

  valueOf(value) {
    const isBitter = !isNaN(value) || typeof value === 'object';

    if(!isBitter || (!value && !isBitter))
      return undefined;

    return value;
  }
}

module.exports = BitterType;
