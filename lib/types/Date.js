'use strict';

const Type = require('./Type');

class DateType extends Type {
  static type(table, name) {
    return table.date(name);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const regexp = /^\d{4}-\d{2}-\d{2}$/;

    if(!value || !regexp.test(value))
      return undefined;

    return value;
  }
}

module.exports = DateType;
