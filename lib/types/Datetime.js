'use strict';

const Type = require('./Type');

class DatetimeType extends Type {
  static type(table, name) {
    return table.timestamp(name);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    if(value instanceof Date) return value;

    const regexp = /^\d{4}-\d{2}-\d{2}( |T)([01][0-9]|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)(\.\d{3}Z)?$/;

    if(!value || !regexp.test(value))
      return undefined;

    return new Date(value);
  }
}

module.exports = DatetimeType;
