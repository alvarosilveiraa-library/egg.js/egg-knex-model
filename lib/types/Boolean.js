'use strict';

const Type = require('./Type');

class BooleanType extends Type {
  static type(table, name) {
    return table.boolean(name);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const isBoolean = typeof value === 'boolean' || typeof value === 'number';

    if(!isBoolean || (!value && !isBoolean))
      return undefined;

    return value ? 1 : 0;
  }
}

module.exports = BooleanType;
