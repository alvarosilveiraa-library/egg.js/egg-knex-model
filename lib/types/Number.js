'use strict';

const Type = require('./Type');

class NumberType extends Type {
  static type(table, name) {
    return table.integer(name);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const isNumber = !isNaN(value);

    if(!isNumber || (!value && !isNumber))
      return undefined;

    return Number(value);
  }
}

module.exports = NumberType;
