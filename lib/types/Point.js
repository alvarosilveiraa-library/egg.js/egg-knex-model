'use strict';

const Type = require('./Type');

class PointType extends Type {
  static type(table, name) {
    return table.specificType(name, 'POINT');
  }

  constructor(options = {}) {
    super(options);
  }

  method(method) {
    return {
      select: value => this.valueOf(this.onValue(value)),
      insert: (value, knex) => this.raw(this.onValue(value), knex),
      update: (value, knex) => this.raw(this.onValue(value), knex),
      delete: value => this.valueOf(this.onValue(value))
    }[method];
  }

  raw(value, knex) {
    const valueOf = this.valueOf(value);

    if(typeof valueOf !== 'object') return valueOf;

    return knex.raw(`POINT(${valueOf.x}, ${valueOf.y})`);
  }

  valueOf(value) {
    const isPoint = (value !== null && typeof value === 'object') && value.x && value.y;

    if(!value || !isPoint)
      return undefined;

    return value;
  }
}

module.exports = PointType;
