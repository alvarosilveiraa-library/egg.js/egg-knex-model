'use strict';

const Type = require('./Type');

class EmailType extends Type {
  static type(table, name, maxLength = 255) {
    return table.string(name, maxLength);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const regexp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    if(!value || !regexp.test(value))
      return undefined;

    return value;
  }
}

module.exports = EmailType;
