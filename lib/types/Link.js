'use strict';

const Type = require('./Type');

class LinkType extends Type {
  static type(table, name, maxLength = 255) {
    return table.string(name, maxLength);
  }

  constructor(options = {}) {
    super(options);
  }

  valueOf(value) {
    const regexp = /^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

    if(!value || !regexp.test(value))
      return undefined;

    return value;
  }
}

module.exports = LinkType;
