'use strict';

const Type = require('./Type');
const crypto = require('crypto');

const DEFAULT_COMMA = '.';

const DEFAULT_SECRET = '12345678912345678912345678912345';

const DEFAULT_ALGORITHM = 'aes-256-ctr';

function encrypt(value, secret = DEFAULT_SECRET, algorithm = DEFAULT_ALGORITHM, comma = DEFAULT_COMMA) {
  if(!value) return value;

  const iv = Buffer.from(crypto.randomBytes(16));

  const cipher = crypto.createCipheriv(algorithm, secret, iv);

  let encrypted = cipher.update(value, 'utf8', 'hex');

  encrypted += cipher.final('hex');

  return `${iv.toString('hex')}${comma}${encrypted.toString()}`;
}

function decrypt(value, secret = DEFAULT_SECRET, algorithm = DEFAULT_ALGORITHM, comma = DEFAULT_COMMA) {
  if(!value) return value;

  const parsed = value.split(comma);

  const iv = Buffer.from(parsed.shift(), 'hex');

  const encrypted = Buffer.from(parsed.join(comma), 'hex');

  const decipher = crypto.createDecipheriv(algorithm, secret, iv);

  let decrypted = decipher.update(encrypted, 'hex', 'utf8');

  decrypted += decipher.final('utf8');

  return decrypted.toString();
}

class PasswordType extends Type {
  static type(table, name, maxLength = 255) {
    return table.string(name, maxLength);
  }

  constructor(options = {}) {
    super(options);

    this.comma = options.comma || DEFAULT_COMMA;

    this.secret = options.secret || DEFAULT_SECRET;

    this.algorithm = options.algorithm || DEFAULT_ALGORITHM;
  }

  method(method) {
    return {
      select: value => this.decrypt(this.onValue(value)),
      insert: value => this.encrypt(this.onValue(value)),
      update: value => this.encrypt(this.onValue(value)),
      delete: value => this.valueOf(this.onValue(value))
    }[method];
  }

  encrypt(value) {
    const valueOf = this.valueOf(value);

    if(typeof valueOf !== 'string') return valueOf;

    return encrypt(valueOf, this.secret, this.algorithm, this.comma);
  }

  decrypt(value) {
    const valueOf = this.valueOf(value);

    if(typeof valueOf !== 'string') return valueOf;

    return decrypt(valueOf, this.secret, this.algorithm, this.comma);
  }

  valueOf(value) {
    if(!value)
      return undefined;

    return value;
  }
}

exports = module.exports = PasswordType;

exports.DEFAULT_COMMA = DEFAULT_COMMA;

exports.DEFAULT_SECRET = DEFAULT_SECRET;

exports.DEFAULT_ALGORITHM = DEFAULT_ALGORITHM;

exports.encrypt = encrypt;

exports.decrypt = decrypt;
