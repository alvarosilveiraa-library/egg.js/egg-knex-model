'use strict';

const Type = require('./Type');
const ServerError = require('../ServerError');

class ObjectType extends Type {
  constructor(options = {}) {
    super(options);

    if(!options.model)
      throw new ServerError({
        status: 500,
        message: '\'model\' is required'
      });

    this.model = options.model;
  }
}

module.exports = ObjectType;
