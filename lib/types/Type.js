'use strict';

const ServerError = require('../ServerError');

class Type {
  static type() {
    throw new ServerError({
      status: 500,
      message: 'Method \'type\' not implemented'
    });
  }

  constructor({
    check = [],
    selectable = true,
    insertable = true,
    updatable = true,
    deletable = false,
    required = false,
    onValue = value => value
  }) {
    this.check = check;

    this.selectable = selectable;

    this.insertable = insertable;

    this.updatable = updatable;

    this.deletable = deletable;

    this.required = required;

    this.onValue = onValue;
  }

  method(method) {
    return {
      select: value => this.valueOf(this.onValue(value)),
      insert: value => this.valueOf(this.onValue(value)),
      update: value => this.valueOf(this.onValue(value)),
      delete: value => this.valueOf(this.onValue(value))
    }[method];
  }

  valueOf() {
    throw new ServerError({
      status: 500,
      message: 'Method \'valueOf\' not implemented'
    });
  }

  validate(value) {
    if(!value && this.required)
      throw new ServerError({
        status: 400,
        message: `Value '${value}' required`
      });

    if(value && this.check.length) {
      const check = this.check.find(check => check === value);

      if(!check)
        throw new ServerError({
          status: 400,
          message: `Value '${value}' check`
        });
    }
  }
}

module.exports = Type;
