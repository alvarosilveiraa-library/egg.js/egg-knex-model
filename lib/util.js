'use strict';

const fs = require('fs');
const path = require('path');

const readDirectory = (dir, request = [ 'directory', 'file' ]) => {
  return fs.readdirSync(dir)
    .reduce((files, file) => {
      const filepath = path.join(dir, file);

      const isDirectory = fs.statSync(filepath).isDirectory();

      if(!isDirectory && request.find(item => item === 'file'))
        files = files.concat({
          filepath,
          isDirectory
        });

      if(isDirectory && request.find(item => item === 'directory')) {
        files = files.concat({
          filepath,
          isDirectory
        });

        files = files.concat(readDirectory(filepath, request));
      }

      return files;
    },
    []
    );
};

exports.readDirectory = readDirectory;
