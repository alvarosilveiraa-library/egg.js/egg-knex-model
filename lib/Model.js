'use strict';

const {
  ObjectType
} = require('./types');

const ENABLE_METHODS = {
  select: 'selectable',
  insert: 'insertable',
  update: 'updatable',
  delete: 'deletable'
};

class Model {
  constructor(knex, types) {
    this.knex = knex;

    this.types = types;
  }

  row(row, method) {
    const result = {};

    for(const key in this.fields) {
      const value = row[key];

      const type = this.fields[key];

      if(!type[ENABLE_METHODS[method]]) continue;

      if(type instanceof ObjectType) {
        const model = new type.model(this.knex);

        const newvalue = model[method](value);

        if(newvalue !== undefined) result[key] = newvalue;

        continue;
      }

      type.validate(value);

      const newvalue = type.method(method)(value, this.knex);

      if(newvalue !== undefined) result[key] = newvalue;
    }

    return result;
  }

  remove(fields, remove) {
    remove.forEach(field => delete fields[field]);

    return fields;
  }

  parse(body, method, remove) {
    if(!body || typeof body !== 'object') return null;

    if(Array.isArray(body))
      return body.map(row => this.remove(this.row(row, method), remove));

    return this.remove(this.row(body, method), remove);
  }

  createTable() {
    const knex = this.knex;

    return knex.schema.hasTable(this.table)
      .then(hasTable => {
        if(!hasTable)
          return knex.schema.createTable(this.table, table => {
            if(typeof this.onCreateTable === 'function')
              this.onCreateTable(table);
          });
      });
  }

  select(body, remove = []) {
    return this.parse(body, 'select', remove);
  }

  insert(body, remove = []) {
    return this.parse(body, 'insert', remove);
  }

  update(body, remove = []) {
    return this.parse(body, 'update', remove);
  }

  delete(body, remove = []) {
    return this.parse(body, 'delete', remove);
  }

  where(body, remove = []) {
    if(
      !body
      || typeof body !== 'object'
      || Array.isArray(body)
    ) return null;

    const result = {};

    for(const key in body) {
      if(body[key] === undefined) continue;

      result[key] = body[key];
    }

    return this.remove(result, remove);
  }
}

module.exports = Model;
