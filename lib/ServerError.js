'use strict';

class ServerError extends Error {
  constructor(error) {
    super();

    this.status = error.status || 500;

    this.message = error.message || 'Internal Error';
  }
}

module.exports = ServerError;
